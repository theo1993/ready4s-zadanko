import { Component } from '@angular/core';
import { AppService } from './app.service';

import { country, currencyExchange } from './app.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public countriesArr: currencyExchange[] = [];
  public slideConfig = {
    "slidesToShow": 1, 
    "slidesToScroll": 1,
    "autoplay": false
  };
  public fullUrl: string = window.location.href;

  private _fromTo = [
    { from: 'GBP', to: 'EUR', imageUrl: 'assets/images/eur.jpg' },
    { from: 'CHF', to: 'USD', imageUrl: 'assets/images/usd.jpg' },
    { from: 'USD', to: 'GBP', imageUrl: 'assets/images/gbp.jpg' }
  ]
  
  constructor(private _appService: AppService) {
    this._appService.getCountries().subscribe((res) => {
      this.getCountriesExchange(res);
    });
  }

  /**
   * Utworzenie tablicy z potrzebnymi danymi
   * @param  {any} res
   * @returns void
   */
  getCountriesExchange(res: any): void {
    const countries = res.rates;
    const countriesWithoutImg: country[] = [];
    const time: Date = new Date(res.timestamp);
    const date: Date = res.date;

    const countriesArr = [];
    
    for (let country in countries) {
      countriesWithoutImg.push({
        countryName: country,
        value: countries[country]
      });
    }

    countriesWithoutImg.forEach(item => this._fromTo.forEach(item2 => {
      if(item.countryName.includes(item2.from)) {
        countriesArr.push({
          from: item2.from,
          to: item2.to,
          valueExchange: Number(item.value),
          imageUrl: this.fullUrl + item2.imageUrl
        })
      }
    }));

    countriesWithoutImg.forEach(item => countriesArr.forEach(item2 => {
      if(item2.to.includes(item.countryName)) {
        let calculateCurrency: number = item2.valueExchange/ item.value;

        this.countriesArr.push({
          from: item2.from,
          to: item2.to,
          valueExchange: calculateCurrency,
          imageUrl: item2.imageUrl,
          time: time.getHours() + ':' + this.timeWithLeadingZeros(time.getMinutes()) + ':' + this.timeWithLeadingZeros(time.getSeconds()),
          date: date
        });
      }
    }));
  }

  /**
   * Dodanie "0" jezeli minuty lub sekundy sa mniejsze od 10
   * @param  {number} value
   * @returns string
   */
  timeWithLeadingZeros(value: number): string {
    return (value < 10 ? '0' : '') + value;
  }

  /**
   * Przypisanie nowych danych po zmianie slidera
   * @returns void
   */
  changeSlide(): void {
    this._appService.getCountries().subscribe((res) => {
      this.getCountriesExchange(res);
    })
  }
}
