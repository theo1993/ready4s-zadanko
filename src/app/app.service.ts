import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class AppService {
  private _accesKey: string = '0798fedf8b939e4c97806af129688d5a';  
  private _countires: string = 'GBP,CHF,EUR,USD';

  constructor(private _http: HttpClient) { }
  
  /**
   * Pobranie danych z api
   * @returns Observable
   */
  getCountries(): Observable<any> {
    const query = this._http.get(`http://data.fixer.io/api/latest?access_key=${this._accesKey}&format=1&base=EUR&symbols=${this._countires}`);
    return query;
  }
}
