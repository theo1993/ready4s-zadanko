export interface country {
	countryName: string,
  value: number
}

export interface currencyExchange {
  from: string,
  to: string,
  valueExchange: number,
  imageUrl: string,
  time: string,
  date: Date
}
